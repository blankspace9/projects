# RESUME

## Projects

1. Бэкенд аудио-стримингового веб-приложения
https://gitlab.com/blankspace9/music-streaming-service.git  
(Клиент https://gitlab.com/blankspace9/music-streaming-client.git)

Данный проект является выпускной квалификационной работой.

Функциональные особенности приложения:

- JWT авторизация, наличие нескольких ролей пользователей
- Клиент-серверное взаимодействие по HTTP протоколу
- Прослушивание, поиск, управление музыкальным воспроизведением
- Создание и редактирование плейлистов
- Поиск контента по каталогу
- Загрузка новых релизов

Основные технологии:

- REST API (net/http, gorilla/mux)
- PostgreSQL (database/sql, pq)
- MongoDB (mongo)
- JWT (jwt)
- Конфигурация (yml, envconf, viper)
- Логирование (logrus)
- Чистая архитектура
- Хеширование данных (bcrypt)

2. Сервис динамической сегментации пользователей
https://gitlab.com/blankspace9/dynamic-user-segmentation.git  

Используемые технологии:

- PostgreSQL
- Docker
- gorilla/mux
- pq
- logrus
- viper
- testify, sqlmock

3. Сервис автоматизированного парсинга .tsv файлов из директории
https://gitlab.com/blankspace9/biocad-parser

Используемые технологии:
- PostgreSQL
- gorilla/mux
- pq
- logrus
- viper

4. gRPC сервер медицинской информационной системы выдачи электронных рецептов
https://gitlab.com/blankspace9/mis-pr

Используемые технологии:
- PostgreSQL
- gRPC (запрос-ответ + потоки)
- JWT
- slog